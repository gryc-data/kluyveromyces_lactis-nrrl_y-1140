## *Kluyveromyces lactis* NRRL Y-1140

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA13835](https://www.ebi.ac.uk/ena/browser/view/PRJNA13835)
* **Assembly accession**: [GCA_000002515.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000002515.1)
* **Original submitter**: Genolevures Consortium

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM251v1
* **Assembly length**: 10,689,156
* **#Chromosomes**: 6
* **Mitochondiral**: No
* **N50 (L50)**: 1,753,957 (3)

### Annotation overview

* **Original annotator**: Genolevures Consortium
* **CDS count**: 5076
* **Pseudogene count**: 33
* **tRNA count**: 163
* **rRNA count**: 12
* **Mobile element count**: 10
