# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2021-04-28)

### Edited

* Build feature hierarchy.

## v1.0 (2021-04-28)

### Added

* The 6 annotated chromosomes of Kluyveromyces lactis NRRL Y-1140 (source EBI, [GCA_000002515.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000002515.1)).
